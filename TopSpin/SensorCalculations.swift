//
//  SensorCalculations.swift
//  TopSpin
//
//  Created by Philip on 15.11.19.
//  Copyright © 2019 SDU. All rights reserved.
//

import Foundation
import CoreBluetooth
import CoreFoundation
import AVFoundation

class SensorCalculations {
    
    var startTime = CFAbsoluteTimeGetCurrent()
    var SavedTimeSwing = CFAbsoluteTimeGetCurrent()

    var maxAccValue = 0.0
    var totalHitsValue = 0
    var SwingSpeedArray = [0.0,0.0]
    public var SavedSpeeds = [Double]()
    
    static var trainingActive = false
    
    static var lastSpeedsArray = [0.0]
    
    func accelerations(from characteristic: CBCharacteristic) -> Array<Double> {
        guard let characteristicData = characteristic.value else { return [0.0] }
        
        let g_pr_lsb = 0.000244
        var g_xdata = 0.0
        var g_ydata = 0.0
        var g_zdata = 0.0
        
        let byteArray = [UInt8](characteristicData)
        //        let byte1: UInt8 = byteArray[2]
        //        let byte2: UInt8 = byteArray[3]
        let accXBit16 = (Int16(byteArray[3]) << 8) + Int16(byteArray[2])
        let accYBit16 = (Int16(byteArray[5]) << 8) + Int16(byteArray[4])
        let accZBit16 = (Int16(byteArray[7]) << 8) + Int16(byteArray[6])
        
        g_xdata = g_pr_lsb * (Double(accXBit16))/4
        g_ydata = g_pr_lsb * (Double(accYBit16))/4
        g_zdata = g_pr_lsb * (Double(accZBit16))/4
        
        //print("\(g_xdata),\(g_ydata),\(g_zdata);")
        
        let g_forceArray = [g_xdata,g_ydata,g_zdata]
        return g_forceArray
    }
    
    func battery(from characteristic:CBCharacteristic) -> Double {
        
        guard let characteristicData2 = characteristic.value else { return 0.0 }
        
        let byteArray2 = [UInt8](characteristicData2)
        //        let byte1: UInt8 = byteArray[2]
        //        let byte2: UInt8 = byteArray[3]
        let battBit16 = Double((Int16(byteArray2[1]) << 8) + Int16(byteArray2[0]))
        
        let battVolt = (battBit16/1000.0)
        let battPerc = 1 - ((4.2 - battVolt)/(4.2 - 3.0))
        
        print("Battery Byte: \(battPerc)%")
        
        
        return battPerc
    }

    
    func swingSpeed(accelerationZ: Double) -> [Double] {
        
        let currentTimeSwing = CFAbsoluteTimeGetCurrent()
        
         
        
        if accelerationZ > 1 {
            SwingSpeedArray[1] = accelerationZ
            
            print("1:\(SwingSpeedArray)")
            
            if SwingSpeedArray[1] < SwingSpeedArray[0] && (currentTimeSwing-SavedTimeSwing) > 0.6 {
                
                SavedTimeSwing = CFAbsoluteTimeGetCurrent()
                
                SavedSpeeds.append(SwingSpeedArray[0])
                
                print("2:\(SavedSpeeds)")
                SwingSpeedArray[0] = 0.0
                SwingSpeedArray[1] = 0.0
            }
            
            if (currentTimeSwing-SavedTimeSwing) < 0.4 {
                
                SwingSpeedArray[0] = 0.0
                SwingSpeedArray[1] = 0.0
                
            }
            
            SwingSpeedArray[0] = SwingSpeedArray[1]
        }
        
        //let lastNumber = SavedSpeeds.last ?? 0.0
        
        SensorCalculations.lastSpeedsArray = SavedSpeeds
        
        return SavedSpeeds
    }
    
    func getTrainingActive(trainingActiveForFunc: Bool) {
        SensorCalculations.trainingActive = trainingActiveForFunc
        
    }
    
    // prior implementation
    
    
    //    func totalHitCounter(accelerationCounter: Double, trainingActive: Bool)-> Int  {
    //
    //        let currentTime = CFAbsoluteTimeGetCurrent()
    //
    //        if trainingActive == false {
    //            totalHitsValue = 0
    //            return totalHitsValue
    //        }
    //
    //        if accelerationCounter > 5.0 && (currentTime - startTime) > 0.6 {
    //            totalHitsValue += 1
    //            startTime = CFAbsoluteTimeGetCurrent()
    //
    //        }
    //        return totalHitsValue
    //    }
        
    //    func maxAcceleration(accelerationCounter: Double, trainingActive: Bool) -> Double {
    //
    ////        if trainingActive == false {
    ////            maxAccValue = 0.0
    ////            return maxAccValue
    ////        }
    //
    //        if accelerationCounter > maxAccValue {
    //        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
    //            maxAccValue = accelerationCounter
    //        }
    //        return maxAccValue
    //    }
}


