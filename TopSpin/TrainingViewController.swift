//
//  ViewController.swift
//  TopSpin
//
//  Created by Philip on 14.10.19.
//  Copyright © 2019 SDU. All rights reserved.
//

import UIKit
import SwiftUI
import CoreMotion
import CoreBluetooth
import CoreData
import AVFoundation

class TrainingViewController: UIViewController {
    
    @IBOutlet weak var StartButton: UIButton!
    @IBOutlet weak var batteryButton: UIButton!
    
    @IBOutlet weak var connectedLabel: UILabel!
    @IBOutlet weak var totalHits: UILabel!
    
    @IBOutlet weak var lastHitAcceleration: UILabel!
    @IBOutlet weak var speedZ: UILabel!
    @IBOutlet weak var speedY: UILabel!
    @IBOutlet weak var maxAcc: UILabel!
    
    @IBOutlet weak var accYLabel: UILabel!
    @IBOutlet weak var accZLabel: UILabel!
    
 
    
    //self.performSegue(withIdentifier: "TrainingViewToStatisticsView", sender: self)
    
    public let SDUSensorCalc = SensorCalculations()
    
    public var swingSpeedArrayText = [Double]()
    
    var centralManager: CBCentralManager!
    var accelerationPeripheral: CBPeripheral!
    
    public static var trainingActive = false
    public var details = false
    
    var maxAccValue = 0.0
    
    var ButtonCounter = 0
    
    let kXAccCharUUID = CBUUID(string: "A2C70031-8F31-11E3-B148-0002A5D5C51B")
    let kYAccCharUUID = CBUUID(string: "A2C70032-8F31-11E3-B148-0002A5D5C51B")
    let kZAccCharUUID = CBUUID(string: "A2C70033-8F31-11E3-B148-0002A5D5C51B")
    let kBattCharUUID = CBUUID(string: "A2C70038-8F31-11E3-B148-0002A5D5C51B")
    let kBandCizerServiceUUID = CBUUID(string: "A2C70010-8F31-11E3-B148-0002A5D5C51B")
    var appDelegate: AppDelegate!
    var context: NSManagedObjectContext!
    
    @IBAction func StartButton(_ sender: UIButton) {
        
        if ButtonCounter == 0 || ButtonCounter % 2 == 0 {
            
            StartButton.backgroundColor = UIColor.red
            StartButton.setTitle("STOP TRAINING", for: .normal)
            TrainingViewController.trainingActive = true
            SDUSensorCalc.getTrainingActive(trainingActiveForFunc: TrainingViewController.trainingActive)
            
        } else {
            
            StartButton.backgroundColor = UIColor.green
            StartButton.setTitle("START TRAINING", for: .normal)
            TrainingViewController.trainingActive = false
            maxAcc.text = "0.000"
            lastHitAcceleration.text = "0.000"
            SDUSensorCalc.maxAccValue = 0.0
            totalHits.text = "0"
            SDUSensorCalc.SavedSpeeds.removeAll()
            
            //SDUSensorCalc.SavedSpeeds.append(0.0)
            
            // this had been the starting point of an database
            
            //            let user = User(context: context)
            //            user.swingSpeedArray = swingSpeedArrayText as NSObject
            
            //            let ue = User(context: context)
            //            ue.swingSpeedAttribute = 3.0
            //            let userEntity = NSEntityDescription.entity(forEntityName: "User", in: context)!
            
            //            let user = NSManagedObject(entity: userEntity, insertInto: context)
            //
            //            user.setValue(swingSpeedArrayText,forKey: "swingSpeedAttribute")
            //
            
        }
        ButtonCounter+=1
        
        
    }
    
    
    @IBAction func showDetails(_ sender: UIButton) {
        
        if accYLabel.isHidden == true {
            
            accYLabel.isHidden = false
            accZLabel.isHidden = false
            speedY.isHidden = false
            speedZ.isHidden = false
            sender.setTitle("Hide details", for: [])
            
            
        } else {
            
            accYLabel.isHidden = true
            accZLabel.isHidden = true
            speedY.isHidden = true
            speedZ.isHidden = true
            sender.setTitle("Show details", for: [])
            
        }
    }
    
    //We dont use this
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "TrainingViewToStatisticsView"){
            let statisticsVC = segue.destination as! StatisticsUIViewController
            
            statisticsVC.statisticsArray = swingSpeedArrayText
            
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        totalHits.text = "0"
        
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        context = appDelegate.persistentContainer.viewContext
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    
}

extension TrainingViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {
        case .unknown:
            print("central.state is .unknown")
        case .resetting:
            print("central.state is .resetting")
        case .unsupported:
            print("central.state is .unsupported")
        case .unauthorized:
            print("central.state is .unauthorized")
        case .poweredOff:
            print("central.state is .poweredOff")
        case .poweredOn:
            print("central.state is .poweredOn")
            centralManager.scanForPeripherals(withServices: nil, options: nil)
            
        @unknown default:
            print("DEFAULT")
        }
        
        
    }
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
                        advertisementData: [String: Any], rssi RSSI: NSNumber) {
        
        
        let deviceName = NSString("BC05FC978B7510AB")
        
        let nameOfDeviceFound = (advertisementData as NSDictionary).object(forKey: CBAdvertisementDataLocalNameKey) as? NSString
        
        
        print("\(String(describing: nameOfDeviceFound))")
        
        if nameOfDeviceFound == deviceName  {
            // Update Status Label
            self.connectedLabel.text = "SDU Sensor connected"
            
            // Stop scanning
            self.centralManager.stopScan()
            
            // Set as the peripheral to use and establish connection
            self.accelerationPeripheral = peripheral
            accelerationPeripheral.delegate = self
            self.centralManager.connect(peripheral, options: nil)
        }
        else {
            
            self.connectedLabel.text = "NOT Found"
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("connected")
        print(peripheral)
        peripheral.discoverServices([kBandCizerServiceUUID])
        
        
    }
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverServices error: NSError!) {
        
        for service in peripheral.services! {
            let thisService = service as CBService
            if service.uuid == kXAccCharUUID {
                // Discover characteristics of bandCizer thing
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
            //Uncomment to print list of UUIDs
            print(thisService.uuid)
        }
    }
    
    
    
}

extension TrainingViewController: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else { return}
        
        for service in services {
            print(service)
            print(service.characteristics ?? "characteristics are nil")
            peripheral.discoverCharacteristics(nil, for: service)
            
        }
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService,
                    error: Error?) {
        guard let characteristics = service.characteristics else { return }
        
        for characteristic in characteristics {
            print(characteristic)
            
            peripheral.readValue(for: characteristic)
            
            peripheral.setNotifyValue(true, for: characteristic)
            
        }
        
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        
        switch characteristic.uuid {
        case kXAccCharUUID:
            //print(characteristic.value ?? "no value")
            
            let accX = SDUSensorCalc.accelerations(from: characteristic)
            onAccelerationsReceived(accX)
            
        case kBattCharUUID:
            print("Battery\(characteristic)")
            
            let battState = SDUSensorCalc.battery(from: characteristic)
            let batteryButtonTitle = String(format:"%.0f",battState*100.0)
            
            if battState < 0.4 {
                
                batteryButton.backgroundColor = UIColor.red
                
            }
            
            batteryButton.setTitle("\(batteryButtonTitle) %", for: [])
 
        default:
            _=0 //print("")//Unhandled Characteristic UUID: \(characteristic.uuid)")
        }
    }
    
    func onAccelerationsReceived(_ accelerationArray: Array<Double>) {
               
               if speedZ.isHidden == false {
                   
                   speedZ.text = String(format: "%.3f",accelerationArray[2])
                   speedY.text = String(format: "%.3f",accelerationArray[1])
                   
               }
               
               if TrainingViewController.trainingActive == true {
            
                   swingSpeedArrayText = SDUSensorCalc.swingSpeed(accelerationZ: accelerationArray[2])
                   lastHitAcceleration.text = String(format:"%.3f",swingSpeedArrayText.last ?? 0.0)
                   maxAccValue = swingSpeedArrayText.max() ?? 0.0
                   maxAcc.text = String(format: "%.3f", maxAccValue)
                   
                   totalHits.text = String(SDUSensorCalc.SavedSpeeds.count)
                   
               }
           }
    
    public func vibrateOnAcc() {
        
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
    
}
//<CBPeripheral: 0x281220a00, identifier = 89E68ECF-7A22-9F27-C581-3106E72197B7, name = TI BLE Sensor Tag, state = disconnected>
