//
//  StatisticsViewController.swift
//  TopSpin
//
//  Created by Philip on 22.11.19.
//  Copyright © 2019 SDU. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI
import Charts


class StatisticsUIViewController: UIViewController {
    
    var statisticsArray: [Double]!
    
    @IBOutlet weak var averageHitSpeed: UILabel!
    @IBOutlet weak var maxHitSpeed: UILabel!
    @IBOutlet weak var totalHits: UILabel! 
    @IBOutlet weak var trainingChart: LineChartView!

     //tabBarController?.selectedIndex = 0
     
    var currentAvg = 0.0
    var arrayLength = 0.0
    var currentMax = 0.0
    var currentTotalHits = 0
        
    override func viewDidLoad() {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        statisticsArray = SensorCalculations.lastSpeedsArray
        makeGraph()
        print(statisticsArray ?? 0)
        
        currentAvg = 0.0
        
        for i in statisticsArray {
            
            currentAvg += i
        
        }
        
        arrayLength = Double(statisticsArray.count)
        currentAvg = (currentAvg/arrayLength)
        currentMax = statisticsArray.max() ?? 0.0
        
        averageHitSpeed.text = String(format: "%.3f",currentAvg)
        maxHitSpeed.text = String(format: "%.3f",currentMax)
        totalHits.text = String(format: "%.0f",arrayLength)
        
    }
    
    func makeGraph() {
        
        var lineChartEntry = [ChartDataEntry]()

        for i in 0..<statisticsArray.count {
         
            let value = ChartDataEntry(x: Double(i), y: statisticsArray[i])
            
            lineChartEntry.append(value)
            
        }

        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Hit accelerations")
        
        line1.colors = [NSUIColor.blue]
        trainingChart.xAxis.labelTextColor = UIColor.white
        
        line1.circleColors = [UIColor.green]
        
        line1.drawCircleHoleEnabled = true
        line1.circleHoleColor = NSUIColor.blue
        
        trainingChart.xAxis.labelPosition = .bottom
        trainingChart.xAxis.drawGridLinesEnabled = false
        
        trainingChart.leftAxis.labelTextColor = UIColor.white
        trainingChart.leftAxis.axisMinimum = 0.0
        trainingChart.leftAxis.axisMaximum = 2.2
        trainingChart.leftAxis.drawAxisLineEnabled = false
        trainingChart.leftAxis.drawGridLinesEnabled = false
        
        trainingChart.drawBordersEnabled = true
        trainingChart.borderColor = UIColor.white
        
        trainingChart.legend.textColor = UIColor.white
        
        trainingChart.chartDescription?.textColor = UIColor.white
        
        
        
        
        
        trainingChart.xAxis.granularity = 1.0
        
        let data = LineChartData()
        data.addDataSet(line1)
        
        trainingChart.data = data
        
    
        
        print(statisticsArray.count)
        
    }
    
}
